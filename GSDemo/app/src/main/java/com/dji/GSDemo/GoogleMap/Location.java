package com.dji.GSDemo.GoogleMap;

public interface Location {
        public static double distance(double lat1, double lat2, double lon1,
                                      double lon2, double el1, double el2) {

            final double R = 6371.54; // Radius of the earth

            double latDistance = Math.toRadians(lat2 - lat1);
            double lonDistance = Math.toRadians(lon2 - lon1);
            double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                    + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                    * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double distance = R * c * 1000; // convert to meters

            double height = el1 - el2;

            distance = Math.pow(distance, 2) + Math.pow(height, 2);

            return Math.sqrt(distance);
        }

    public class GPSCoor {
        private double[] values = new double[3];
        public GPSCoor(double lat, double lon, double alt) {
            values[0] = lat;
            values[1] = lon;
            values[2] = alt;
        }
        public double getLat(){
            return values[0];
        }
        public double getLon(){
            return values[1];
        }
        public double getElev(){
            return values[2];
        }
    }

    }

