package com.dji.GSDemo.GoogleMap;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.SurfaceTexture;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import java.io.FileReader;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import dji.common.mission.waypoint.Waypoint;
import dji.common.mission.waypoint.WaypointMissionFinishedAction;
import dji.common.mission.waypoint.WaypointMissionHeadingMode;

import dji.common.useraccount.UserAccountState;
import dji.common.util.CommonCallbacks;
import dji.common.error.DJIError;
import dji.internal.util.FileUtils;
import dji.sdk.camera.VideoFeeder;
import dji.sdk.codec.DJICodecManager;
import dji.sdk.useraccount.UserAccountManager;

import dji.common.camera.SettingsDefinitions;
import dji.sdk.base.BaseProduct;
import dji.common.product.Model;
import dji.sdk.camera.Camera;

import dji.common.camera.FocusState;
import android.view.TextureView;

import java.io.File;
import java.io.FileOutputStream;
import android.os.Environment;

import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;


import dji.thirdparty.afinal.core.AsyncTask;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import oksi.coregistration.CoregistrationGrpc;
import oksi.coregistration.Server;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import static dji.sdk.camera.Camera.XT2_IR_CAMERA_INDEX;

public class MainActivity extends FragmentActivity implements DJICodecManager.YuvDataCallback, View.OnClickListener, GoogleMap.OnMapClickListener, OnMapReadyCallback, Location, MissionSupervisor {


    //Create Drone Operator instance for drone flight controller
    MissionSupervisor.RoutePlanner droneOperation = new MissionSupervisor.RoutePlanner();
    protected static final String TAG = "GSDemoActivity";

    //Button variables
    private GoogleMap gMap;
    private Button config, upload, start, reroute, clear;
    private boolean isAdd = false;
    private final Map<Integer, Marker> mMarkers = new ConcurrentHashMap<Integer, Marker>();
    private Marker droneMarker = null;

    //Video Stream Variables
    protected VideoFeeder.VideoDataListener mReceivedVideoDataListener = null;
    protected DJICodecManager mCodecManager = null;
    public FocusState.Builder cameraFocus = new FocusState.Builder();
    private static final int MSG_WHAT_SHOW_TOAST = 0;
    private static final int MSG_WHAT_UPDATE_TITLE = 1;
    private SurfaceHolder.Callback surfaceCallback;
    private enum DemoType { USE_TEXTURE_VIEW, USE_SURFACE_VIEW, USE_SURFACE_VIEW_DEMO_DECODER}
    private static DemoType demoType = DemoType.USE_TEXTURE_VIEW;
    private VideoFeeder.VideoFeed standardVideoFeeder;
    private TextView titleTv;
    public Handler mainHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_WHAT_SHOW_TOAST:
                    Toast.makeText(getApplicationContext(), (String) msg.obj, Toast.LENGTH_SHORT).show();
                    break;
                case MSG_WHAT_UPDATE_TITLE:
                    if (titleTv != null) {
                        titleTv.setText((String) msg.obj);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    //APP surface/texture variables for streaming
    private TextureView videostreamPreviewTtView;
    private SurfaceView videostreamPreviewSf;
    private SurfaceHolder videostreamPreviewSh;
    private Camera mCamera;

    private int videoViewWidth;
    private int videoViewHeight;
    private int count;

    //Display variables
    public DecimalFormat df2 = new DecimalFormat("#0.00000");
    private boolean DISPLAY_UPDATES = false;
    private boolean GRAB_IMAGES = false;
    private boolean INIT_PATH = true;
    private boolean performReroute = false;


    public List<Location.GPSCoor> initRoute = new ArrayList<Location.GPSCoor>();
    public List<Location.GPSCoor> correctedRoute = new ArrayList<Location.GPSCoor>();

    public Location.GPSCoor targetCoordinates;

    //Server-Client grpc
    public Server.CoregistrationResponse responseValue;



    Server.CoregistrationResponse sendCoregistrationRequestToServer(
            byte[] image,
            MissionSupervisor.RoutePlanner routePlanner, Location.GPSCoor targetLocation) throws InvalidProtocolBufferException {
        System.out.println("Sending coregistration request to grpc server...");

        ManagedChannel channel = ManagedChannelBuilder.forAddress("10.45.191.140", 50051).usePlaintext().build();

        CoregistrationGrpc.CoregistrationBlockingStub stub
                = CoregistrationGrpc.newBlockingStub(channel);


        Server.CoregistrationRequest request = createCoregistrationRequest(image, routePlanner, targetLocation);


        Server.CoregistrationResponse response = stub.registerToSatellite(request);

        String responseString = JsonFormat.printer().print(response);

        System.out.println("Server response:");
        System.out.println(responseString);
        return response;
    }


    private static Server.CoregistrationRequest createCoregistrationRequest(
            byte[] image, RoutePlanner routePlanner, Location.GPSCoor targetC) {

        return Server.CoregistrationRequest.newBuilder()
                    .setMissionId("test-mission-1")
                    .setImage(Server.Image.newBuilder().setData(ByteString.copyFrom(image)))
                    .setPoseEstimate(Server.Pose.newBuilder()
                        .setPitch((float) routePlanner.getPitchAngle())
                        .setRoll((float) routePlanner.getRollAngle())
                        .setYaw((float) routePlanner.getYawAngle())
                        .setCoordinate(Server.Coordinate.newBuilder()
                            .setAltitudeM((float) routePlanner.droneLocationAlt)
                            .setLatitude(routePlanner.droneLocationLat)
                            .setLongitude(routePlanner.droneLocationLng)
                        )
                    )
                    .setTargetCoordinate(Server.Coordinate.newBuilder()
                        .setAltitudeM((float) targetC.getElev())
                        .setLatitude(targetC.getLat())
                        .setLongitude(targetC.getLon())
                    )
                    .build();
    }

    /**
     * Create surface/texture view on resume of app
     * Also initialize flight controller
     */
    @Override
    protected void onResume(){
        super.onResume();
        //initPreviewer();
        initSurfaceOrTextureView();
        droneOperation.initFlightController();
        notifyStatusChange();

        Intent intent = getIntent();
        String message_lat = intent.getStringExtra(ConnectionActivity.LAT_MESSAGE);
        String message_lon = intent.getStringExtra(ConnectionActivity.LON_MESSAGE);

        setResultToToast("Target Latitude " + message_lat + "\n" + "Target Longitude " + message_lon);

        //Target is located ~100m south of current drone position
        //targetCoordinates = new Location.GPSCoor(droneOperation.getDroneLocationLat() - 0.00089832, droneOperation.getDroneLocationLng(),0.0);

        double targetLatitudeLocation = Double.parseDouble(message_lat);
        double targetLongitudeLocation = Double.parseDouble(message_lon);

        targetCoordinates = new Location.GPSCoor(targetLatitudeLocation, targetLongitudeLocation, 0.0);


    }



    /**
     * Initialize the type of app surface view
     */
    private void initSurfaceOrTextureView(){
        switch (demoType) {
            case USE_SURFACE_VIEW:
                initPreviewerSurfaceView();
                break;
            case USE_SURFACE_VIEW_DEMO_DECODER:
                /**
                 * we also need init the textureView because the pre-transcoded video steam will display in the textureView
                 */
                initPreviewerTextureView();

                /**
                 * we use standardVideoFeeder to pass the transcoded video data to DJIVideoStreamDecoder, and then display it
                 * on surfaceView
                 */
                initPreviewerSurfaceView();
                break;
            case USE_TEXTURE_VIEW:
                initPreviewerTextureView();
                break;
        }
    }

    /**
     * On app pause, remove the video stream listener
     */
    @Override
    protected void onPause(){

        if (mCamera != null) {
            if (VideoFeeder.getInstance().getPrimaryVideoFeed() != null) {
                VideoFeeder.getInstance().getPrimaryVideoFeed().removeVideoDataListener(mReceivedVideoDataListener);
            }
            if (standardVideoFeeder != null) {
                standardVideoFeeder.removeVideoDataListener(mReceivedVideoDataListener);
            }
        }
        super.onPause();
    }

    /**
     * On app destroy, remove flight control listener
     * and clean app surface
     */

    @Override
    protected void onDestroy(){
        //unregisterReceiver(mReceiver);
        droneOperation.removeListener();

        if (mCodecManager != null) {
            mCodecManager.cleanSurface();
            mCodecManager.destroyCodec();
        }

        super.onDestroy();
    }

    /**
     * Helper function to display text to app
     */
    private void setResultToToast(final String string){
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, string, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Initialize the app, button, ui etc.
     * Also init the video stream surface listeners
     * The variables are in activity_main.xml
     */
    private void initUI() {

        clear = (Button) findViewById(R.id.clear);
        config = (Button) findViewById(R.id.config);
        upload = (Button) findViewById(R.id.upload);
        start = (Button) findViewById(R.id.start);
        reroute = (Button) findViewById(R.id.reroute);

        clear.setOnClickListener(this);
        config.setOnClickListener(this);
        upload.setOnClickListener(this);
        start.setOnClickListener(this);
        reroute.setOnClickListener(this);

        videostreamPreviewTtView = (TextureView)findViewById(R.id.livestream_preview_ttv);
        titleTv = (TextView) findViewById(R.id.title_tv);

        videostreamPreviewSf = (SurfaceView) findViewById(R.id.livestream_preview_sf);
        videostreamPreviewSf.setClickable(true);
        videostreamPreviewSf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float rate = VideoFeeder.getInstance().getTranscodingDataRate();
                setResultToToast("current rate:" + rate + "Mbps");
                if (rate < 10) {
                    VideoFeeder.getInstance().setTranscodingDataRate(10.0f);
                    setResultToToast("set rate to 10Mbps");
                } else {
                    VideoFeeder.getInstance().setTranscodingDataRate(3.0f);
                    setResultToToast("set rate to 3Mbps");
                }
            }
        });
        updateUIVisibility();
    }

    /**
     * Update the UI on the app depending on view
     */
    private void updateUIVisibility(){
        switch (demoType) {
            case USE_SURFACE_VIEW:
                videostreamPreviewSf.setVisibility(View.VISIBLE);
                videostreamPreviewTtView.setVisibility(View.GONE);
                break;
            case USE_SURFACE_VIEW_DEMO_DECODER:
                /**
                 * we need display two video stream at the same time, so we need let them to be visible.
                 */
                videostreamPreviewSf.setVisibility(View.VISIBLE);
                videostreamPreviewTtView.setVisibility(View.VISIBLE);
                break;

            case USE_TEXTURE_VIEW:
                videostreamPreviewSf.setVisibility(View.GONE);
                videostreamPreviewTtView.setVisibility(View.VISIBLE);
                break;
        }
    }
    private long lastupdate;

    /**
     * Track connection status of product.  Also init the
     * callback function for raw video data streaming.  Set
     * the camera we which to track, i.e., the thermal cam
     * and set the view to only thermal so videofeed listener
     * receives only thermal image data.
     */
    private void notifyStatusChange() {

        final BaseProduct product = VideoDecodingApplication.getProductInstance();

        Log.d(TAG, "notifyStatusChange: " + (product == null ? "Disconnect" : (product.getModel() == null ? "null model" : product.getModel().name())));
        if (product != null && product.isConnected() && product.getModel() != null) {
            setResultToToast(product.getModel().name() + " Connected " + demoType.name());
        } else {
            setResultToToast("Disconnected");
        }

        // The callback for receiving the raw H264 video data for camera live view
        mReceivedVideoDataListener = new VideoFeeder.VideoDataListener() {

            @Override
            public void onReceive(byte[] videoBuffer, int size) {
                if (System.currentTimeMillis() - lastupdate > 1000) {
                    Log.d(TAG, "camera recv video data size: " + size);
                    lastupdate = System.currentTimeMillis();
                }
                switch (demoType) {
                    case USE_SURFACE_VIEW:
                        if (mCodecManager != null) {
                            mCodecManager.sendDataToDecoder(videoBuffer, size);
                        }
                        break;
                    case USE_SURFACE_VIEW_DEMO_DECODER:
                        /**
                         we use standardVideoFeeder to pass the transcoded video data to DJIVideoStreamDecoder, and then display it
                         * on surfaceView
                         */
                        DJIVideoStreamDecoder.getInstance().parse(videoBuffer, size);
                        break;

                    case USE_TEXTURE_VIEW:
                        if (mCodecManager != null) {
                            mCodecManager.sendDataToDecoder(videoBuffer, size);
                        }
                        break;
                }

            }
        };

        if (null == product || !product.isConnected()) {
            mCamera = null;
            setResultToToast("Disconnected");
        } else {
            if (!product.getModel().equals(Model.UNKNOWN_AIRCRAFT)) {
                // Visible camera ONLY
                //mCamera = product.getCamera();

                /* Thermal camera grab */
                mCamera = product.getCameraWithComponentIndex(XT2_IR_CAMERA_INDEX);

                mCamera.setMode(SettingsDefinitions.CameraMode.SHOOT_PHOTO, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError != null) {
                            setResultToToast("can't change mode of camera, error:"+djiError.getDescription());
                        }
                    }
                });

                //setResultToToast("Camera Name " + product.getCamera().getDisplayName());

                mCamera.setDisplayMode(SettingsDefinitions.DisplayMode.THERMAL_ONLY, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError != null) {
                            //setResultToToast("Thermal camera:"+djiError.getDescription());
                        }
                    }
                });

                //When calibration is needed or the fetch key frame is required by SDK, should use the provideTranscodedVideoFeed
                //to receive the transcoded video feed from main camera.
                if (demoType == DemoType.USE_SURFACE_VIEW_DEMO_DECODER && isTranscodedVideoFeedNeeded()) {
                    standardVideoFeeder = VideoFeeder.getInstance().provideTranscodedVideoFeed();
                    standardVideoFeeder.addVideoDataListener(mReceivedVideoDataListener);
                    return;
                }
                if (VideoFeeder.getInstance().getPrimaryVideoFeed() != null) {
                    VideoFeeder.getInstance().getPrimaryVideoFeed().addVideoDataListener(mReceivedVideoDataListener);
                }

            }
        }
    }

    private boolean isTranscodedVideoFeedNeeded() {
        if (VideoFeeder.getInstance() == null) {
            return false;
        }

        return VideoFeeder.getInstance().isFetchKeyFrameNeeded() || VideoFeeder.getInstance()
                .isLensDistortionCalibrationNeeded();
    }

    /**
     * Initialize the UI and flight controller listeners
     * on app creation.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        // When the compile and target version is higher than 22, please request the
        // following permissions at runtime to ensure the
        // SDK work well.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                            Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.WAKE_LOCK, Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW,
                            Manifest.permission.READ_PHONE_STATE,
                    }
                    , 1);
        }

        setContentView(R.layout.activity_main);

        IntentFilter filter = new IntentFilter();
        filter.addAction(DJIDemoApplication.FLAG_CONNECTION_CHANGE);
        registerReceiver(mReceiver, filter);

        initUI();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        droneOperation.addListener();



    }

    /**
     * Track if there has been a connection/product change.
     */
    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onProductConnectionChange();
        }
    };

    private void onProductConnectionChange()
    {

        droneOperation.initFlightController();
        loginAccount();
    }

    /**
     * Login prompt
     */

    private void loginAccount(){

        UserAccountManager.getInstance().logIntoDJIUserAccount(this,
                new CommonCallbacks.CompletionCallbackWith<UserAccountState>() {
                    @Override
                    public void onSuccess(final UserAccountState userAccountState) {
                        Log.e(TAG, "Login Success");
                    }
                    @Override
                    public void onFailure(DJIError error) {
                        setResultToToast("Login Error:"
                                + error.getDescription());
                    }
                });
    }

    private void setUpMap() {
        gMap.setOnMapClickListener(this);// add the listener for click for amap object
    }

    @Override
    public void onMapClick(LatLng point) {
        if (isAdd == true){
            markWaypoint(point);
        }else{
            setResultToToast("Error");
        }
    }

    /**
     * Check for lla values in the correct ranges.
     * param latitude
     * @param longitude
     * @return boolean
     */

    public static boolean checkGpsCoordination(double latitude, double longitude) {
        return (latitude > -90 && latitude < 90 && longitude > -180 && longitude < 180) && (latitude != 0f && longitude != 0f);
    }

    /**
     * Function to update drone location if googlemap
     * view is used. Optional.
     */
    private void updateDroneLocation(){

        LatLng pos = new LatLng(droneOperation.droneLocationLat, droneOperation.droneLocationLng);
        //Create MarkerOptions object
        final MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(pos);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.aircraft));

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (droneMarker != null) {
                    droneMarker.remove();
                }

                if (checkGpsCoordination(droneOperation.droneLocationLat, droneOperation.droneLocationLng)) {
                    droneMarker = gMap.addMarker(markerOptions);
                }
            }
        });
    }

    private void markWaypoint(LatLng point){
        //Create MarkerOptions object
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        Marker marker = gMap.addMarker(markerOptions);
        mMarkers.put(mMarkers.size(), marker);
    }

    /**
     * On user click, track the options chosen.
     * User may config the mission, upload the flight path,
     * start the flight path, reroute in mid-air, or stop
     * and clear and the previously uploaded mission.
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear:{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gMap.clear();
                    }

                });
                GRAB_IMAGES = false;
                INIT_PATH = true;
                handleYUVClick();
                droneOperation.clearMission();
                droneOperation.waypointMissionBuilder.waypointList(droneOperation.waypointList);
                droneOperation.stopWaypointMission();
                break;
            }
            case R.id.config:{
                GRAB_IMAGES = true;
                droneOperation.clearMission();
                handleYUVClick();

                showSettingDialog();
                break;
            }
            case R.id.upload:{
                GRAB_IMAGES = true;
                handleYUVClick();
                droneOperation.uploadWayPointMission();
                break;
            }
            case R.id.start:{
                GRAB_IMAGES = true;
                handleYUVClick();
                droneOperation.startWaypointMission();
                break;
            }
            case R.id.reroute:{
                GRAB_IMAGES = true;
                performReroute = !performReroute;
                handleYUVClick();
                //droneOperation.rerouteWaypointMission();
                break;
            }
            default:
                break;
        }
    }

    /**
     * Display the flight mission configuration screen.
     */

    private void showSettingDialog(){
        //initFlightPath();
        //droneOperation.initialMission();

        LinearLayout wayPointSettings = (LinearLayout)getLayoutInflater().inflate(R.layout.dialog_waypointsetting, null);

        RadioGroup speed_RG = (RadioGroup) wayPointSettings.findViewById(R.id.speed);
        RadioGroup actionAfterFinished_RG = (RadioGroup) wayPointSettings.findViewById(R.id.actionAfterFinished);
        RadioGroup heading_RG = (RadioGroup) wayPointSettings.findViewById(R.id.heading);

        speed_RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.lowSpeed){
                    droneOperation.mSpeed = 2.0f;
                } else if (checkedId == R.id.MidSpeed){
                    droneOperation.mSpeed = 5.0f;
                } else if (checkedId == R.id.HighSpeed){
                    droneOperation.mSpeed = 10.0f;
                }
            }

        });

        actionAfterFinished_RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d(TAG, "Select finish action");
                if (checkedId == R.id.finishNone){
                    droneOperation.mFinishedAction = WaypointMissionFinishedAction.NO_ACTION;
                } else if (checkedId == R.id.finishGoHome){
                    droneOperation.mFinishedAction = WaypointMissionFinishedAction.GO_HOME;
                } else if (checkedId == R.id.finishAutoLanding){
                    droneOperation.mFinishedAction = WaypointMissionFinishedAction.AUTO_LAND;
                } else if (checkedId == R.id.finishToFirst){
                    droneOperation.mFinishedAction = WaypointMissionFinishedAction.GO_FIRST_WAYPOINT;
                }
            }
        });

        heading_RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d(TAG, "Select heading");

                if (checkedId == R.id.headingNext) {
                    droneOperation.mHeadingMode = WaypointMissionHeadingMode.AUTO;
                } else if (checkedId == R.id.headingInitDirec) {
                    droneOperation.mHeadingMode = WaypointMissionHeadingMode.USING_INITIAL_DIRECTION;
                } else if (checkedId == R.id.headingRC) {
                    droneOperation.mHeadingMode = WaypointMissionHeadingMode.CONTROL_BY_REMOTE_CONTROLLER;
                } else if (checkedId == R.id.headingWP) {
                    droneOperation.mHeadingMode = WaypointMissionHeadingMode.USING_WAYPOINT_HEADING;
                }
            }
        });

        new AlertDialog.Builder(this)
                .setTitle("")
                .setView(wayPointSettings)
                .setPositiveButton("Finish",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id) {

                        Log.e(TAG,"speed "+ droneOperation.mSpeed);
                        Log.e(TAG, "mFinishedAction "+ droneOperation.mFinishedAction);
                        Log.e(TAG, "mHeadingMode "+ droneOperation.mHeadingMode);
                        droneOperation.configWayPointMission();
                    }

                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                })
                .create()
                .show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (gMap == null) {
            gMap = googleMap;
            setUpMap();
        }

        LatLng oksi = new LatLng(33.851116, -118.286900);

        gMap.addMarker(new MarkerOptions().position(oksi).title("Marker in OKSI"));
        gMap.moveCamera(CameraUpdateFactory.newLatLng(oksi));
    }

    /**
     * Callback function for receiving the raw visible/thermal image
     * data.  It controls the image frame rate (count), and creates
     * a screenshot of the thermal data to (optionally) be saved on phone.
     * Additionally, coordinates with co-registration to set the initial
     * flight path and reroute if necessary.
     */

    @Override
    public void onYuvDataReceived(MediaFormat format, final ByteBuffer yuvFrame, int dataSize, final int width, final int height) {
        //In this demo, we test the YUV data by saving it into JPG files.
        //DJILog.d(TAG, "onYuvDataReceived " + dataSize);


        if (count++ % 30 == 0 && yuvFrame != null) {
            final byte[] bytes = new byte[dataSize];
            yuvFrame.get(bytes);

            try
            {
                    responseValue = sendCoregistrationRequestToServer(bytes, droneOperation, targetCoordinates);
                    Log.d(TAG, "test response " + responseValue.getWaypointsCount());

                    if (INIT_PATH)
                    {
                        for (int wc = 0; wc < responseValue.getWaypointsCount(); wc++)
                        {
                            //Randomize the initial path.  Each waypoint has +/- 2m error
                            Random rand = new Random();
                            int coordinateNoise = rand.nextInt(2);
                            double coordinateErrorDegrees = Double.valueOf(coordinateNoise)/100000; //Convert to degrees

                            double lt = responseValue.getWaypoints(wc).getLatitude() + coordinateErrorDegrees;
                            double ln = responseValue.getWaypoints(wc).getLongitude() + coordinateErrorDegrees;
                            double al = responseValue.getWaypoints(wc).getAltitudeM();

                            Log.d(TAG, "lat " + lt + " long " + ln + " alt " + al);
                            initRoute.add(new Location.GPSCoor(lt, ln, al));

                        }

                        droneOperation.initialMission(initRoute);
                        setResultToToast("INITIAL MISSION SET: " + initRoute.size());
                        INIT_PATH = false;
                    }
                    else
                    {
                        if(responseValue.getRouteUpdate())
                        {
                            for (int wc = 5; wc < responseValue.getWaypointsCount(); wc++) {
                                double lt = responseValue.getWaypoints(wc).getLatitude();
                                double ln = responseValue.getWaypoints(wc).getLongitude();
                                double al = responseValue.getWaypoints(wc).getAltitudeM();

                                Log.d(TAG, "latReroute " + lt + " longReroute " + ln + " altReroute " + al);
                                correctedRoute.add(new Location.GPSCoor(lt, ln, al));
                            }
                            setResultToToast("Rerouting...");
                            droneOperation.rerouteWaypointMission(correctedRoute);
                            performReroute = !performReroute;
                        }

                    }

            } catch (Exception e) {
                setResultToToast("ERRED OUT");
                e.printStackTrace();
            }




            if (DISPLAY_UPDATES) {
                setResultToToast("Lat " + df2.format(droneOperation.getDroneLocationLat()) + "\nLon " + df2.format(droneOperation.getDroneLocationLng()) + "\nAlt " + df2.format(droneOperation.getDroneLocationAlt()) +
                        "\nPitch " + droneOperation.getPitchAngle());
            }

            //DJILog.d(TAG, "onYuvDataReceived2 " + dataSize);
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    // two samples here, it may has other color format.
                    int colorFormat = format.getInteger(MediaFormat.KEY_COLOR_FORMAT);
                    switch (colorFormat) {
                        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
                            //NV12
                            //NOT SUPPORTED
                        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
                            //YUV420P
                            newSaveYuvDataToJPEG420P(bytes, width, height);
                            break;
                        default:
                            break;
                    }

                }
            });
        }
    }

    /**
     * Function that takes video stream data and converts to correct
     * format for subsequent saving to jpeg.
     * @param yuvFrame
     * @param width
     * @param height
     */

    private void newSaveYuvDataToJPEG420P(byte[] yuvFrame, int width, int height) {
        if (yuvFrame.length < width * height) {
            return;
        }
        int length = width * height;

        byte[] u = new byte[width * height / 4];
        byte[] v = new byte[width * height / 4];

        for (int i = 0; i < u.length; i ++) {
            u[i] = yuvFrame[length + i];
            v[i] = yuvFrame[length + u.length + i];
        }
        for (int i = 0; i < u.length; i++) {
            yuvFrame[length + 2 * i] = v[i];
            yuvFrame[length + 2 * i + 1] = u[i];
        }
        //screenShot(yuvFrame, Environment.getExternalStorageDirectory() + "/DJI_ScreenShot", width, height);
    }

    /**
     * Save the buffered data into a JPG image file
     */
    private void screenShot(byte[] buf, String shotDir, int width, int height) {
        File dir = new File(shotDir);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
        YuvImage yuvImage = new YuvImage(buf,
                ImageFormat.NV21,
                width,
                height,
                null);
        OutputStream outputFile;
        final String path = dir + "/ScreenShot_" + System.currentTimeMillis() + ".jpg";
        try {
            outputFile = new FileOutputStream(new File(path));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "test screenShot: new bitmap output file error: " + e);
            return;
        }
        if (outputFile != null) {
            yuvImage.compressToJpeg(new Rect(0,
                    0,
                    width,
                    height), 100, outputFile);
        }
        try {
            outputFile.close();
        } catch (IOException e) {
            Log.e(TAG, "test screenShot: compress yuv image error: " + e);
            e.printStackTrace();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //displayPath(path);
            }
        });
    }


    /**
     * Init a fake texture view to for the codec manager, so that the video raw data can be received
     * by the camera
     */
    private void initPreviewerTextureView() {
        videostreamPreviewTtView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                Log.d(TAG, "real onSurfaceTextureAvailable");
                videoViewWidth = width;
                videoViewHeight = height;
                Log.d(TAG, "real onSurfaceTextureAvailable: width " + videoViewWidth + " height " + videoViewHeight);
                if (mCodecManager == null) {
                    mCodecManager = new DJICodecManager(getApplicationContext(), surface, width, height);
                }
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                videoViewWidth = width;
                videoViewHeight = height;
                Log.d(TAG, "real onSurfaceTextureAvailable2: width " + videoViewWidth + " height " + videoViewHeight);
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                if (mCodecManager != null) {
                    mCodecManager.cleanSurface();
                }
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });
    }

    /**
     * Init a surface view for the DJIVideoStreamDecoder
     */
    private void initPreviewerSurfaceView() {
        videostreamPreviewSh = videostreamPreviewSf.getHolder();
        surfaceCallback = new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                Log.d(TAG, "real onSurfaceTextureAvailable");
                videoViewWidth = videostreamPreviewSf.getWidth();
                videoViewHeight = videostreamPreviewSf.getHeight();
                Log.d(TAG, "real onSurfaceTextureAvailable3: width " + videoViewWidth + " height " + videoViewHeight);
                switch (demoType) {
                    case USE_SURFACE_VIEW:
                        if (mCodecManager == null) {
                            mCodecManager = new DJICodecManager(getApplicationContext(), holder, videoViewWidth,
                                    videoViewHeight);
                        }
                        break;
                    case USE_SURFACE_VIEW_DEMO_DECODER:
                        // This demo might not work well on P3C and OSMO.
                        NativeHelper.getInstance().init();
                        DJIVideoStreamDecoder.getInstance().init(getApplicationContext(), holder.getSurface());
                        DJIVideoStreamDecoder.getInstance().resume();
                        break;
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                videoViewWidth = width;
                videoViewHeight = height;
                Log.d(TAG, "real onSurfaceTextureAvailable4: width " + videoViewWidth + " height " + videoViewHeight);
                switch (demoType) {
                    case USE_SURFACE_VIEW:
                        //mCodecManager.onSurfaceSizeChanged(videoViewWidth, videoViewHeight, 0);
                        break;
                    case USE_SURFACE_VIEW_DEMO_DECODER:
                        DJIVideoStreamDecoder.getInstance().changeSurface(holder.getSurface());
                        break;
                }

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                switch (demoType) {
                    case USE_SURFACE_VIEW:
                        if (mCodecManager != null) {
                            mCodecManager.cleanSurface();
                            mCodecManager.destroyCodec();
                            mCodecManager = null;
                        }
                        break;
                    case USE_SURFACE_VIEW_DEMO_DECODER:
                        DJIVideoStreamDecoder.getInstance().stop();
                        NativeHelper.getInstance().release();
                        break;
                }

            }
        };

        videostreamPreviewSh.addCallback(surfaceCallback);
    }

    /**
     * The callback function to receive raw video data.
     */

    private void handleYUVClick() {
        if (GRAB_IMAGES){
            switch (demoType) {
                case USE_TEXTURE_VIEW:
                case USE_SURFACE_VIEW:
                    mCodecManager.enabledYuvData(true);
                    mCodecManager.setYuvDataCallback(this::onYuvDataReceived);
                    break;
                case USE_SURFACE_VIEW_DEMO_DECODER:
                    DJIVideoStreamDecoder.getInstance().changeSurface(null);
                    DJIVideoStreamDecoder.getInstance().setYuvDataListener(MainActivity.this);
                    break;
            }
        }
        else
        {
            mCodecManager.enabledYuvData(false);
            mCodecManager.setYuvDataCallback(null);
        }
    }


}
