package com.dji.GSDemo.GoogleMap;

import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import dji.common.camera.SettingsDefinitions;
import dji.common.error.DJIError;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.gimbal.GimbalMode;
import dji.common.gimbal.GimbalState;
import dji.common.gimbal.Rotation;
import dji.common.gimbal.RotationMode;
import dji.common.mission.waypoint.Waypoint;
import dji.common.mission.waypoint.WaypointMission;
import dji.common.mission.waypoint.WaypointMissionDownloadEvent;
import dji.common.mission.waypoint.WaypointMissionExecutionEvent;
import dji.common.mission.waypoint.WaypointMissionFinishedAction;
import dji.common.mission.waypoint.WaypointMissionFlightPathMode;
import dji.common.mission.waypoint.WaypointMissionHeadingMode;
import dji.common.mission.waypoint.WaypointMissionUploadEvent;
import dji.common.util.CommonCallbacks;
import dji.sdk.base.BaseProduct;
import dji.sdk.camera.Camera;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.gimbal.Gimbal;
import dji.sdk.mission.waypoint.WaypointMissionOperator;
import dji.sdk.mission.waypoint.WaypointMissionOperatorListener;
import dji.sdk.products.Aircraft;
import dji.sdk.products.HandHeld;
import dji.sdk.sdkmanager.DJISDKManager;


import static dji.common.mission.waypoint.WaypointMissionState.READY_TO_EXECUTE;
import static dji.common.mission.waypoint.WaypointMissionState.READY_TO_UPLOAD;
import static dji.common.mission.waypoint.WaypointMissionState.UPLOADING;

public interface MissionSupervisor {

    public class RoutePlanner {
        /** Flight control/waypoint parameters */
        public double droneLocationLat, droneLocationLng, droneLocationAlt;
        public double targetLat, targetLon, targetAlt;
        private FlightController mFlightController;
        public static WaypointMission.Builder waypointMissionBuilder;
        public float mSpeed;
        public WaypointMissionFinishedAction mFinishedAction = WaypointMissionFinishedAction.NO_ACTION;
        public WaypointMissionHeadingMode mHeadingMode = WaypointMissionHeadingMode.AUTO;

        private Handler handler = new Handler();
        /** Gimbal pose values */
        private Gimbal gimbal = null;
        private double pitchAngle = 0;
        private double yawAngle = 0;
        private double rollAngle = 0;
        protected static final String TAG2 = "missionSupervisor";

        /** Route waypoint mission */
        public List<Waypoint> waypointList = new ArrayList<>();
        private WaypointMissionOperator instance;

        private final Activity mActivity = new Activity();

        public RoutePlanner() {
            droneLocationLat = 181;
            droneLocationLng = 181;
            droneLocationAlt = 0;
            targetAlt = 0;
            targetLat = 0;
            targetLon = 0;
            mSpeed = 10;

        }

        /** Initialize the flight controller and set the gimbal mode
         * which allows for user control
         */
        public void initFlightController() {

            BaseProduct product = DJIDemoApplication.getProductInstance();
            if (product != null && product.isConnected()) {
                if (product instanceof Aircraft) {
                    mFlightController = ((Aircraft) product).getFlightController();
                    gimbal = ((Aircraft) product).getGimbals().get(0);
                    gimbal.setMode(GimbalMode.FPV, null);

                }
                else{
                    mFlightController = ((Aircraft) product).getFlightController();
                    gimbal = product.getGimbal();
                    gimbal.setMode(GimbalMode.FPV, null);
                }
            }

            if (mFlightController != null) {
                mFlightController.setStateCallback(new FlightControllerState.Callback() {
                    /** Retrieve the drone's current pose.
                     * Also set the gimbal to point to target location
                     */
                    @Override
                    public void onUpdate(FlightControllerState djiFlightControllerCurrentState) {
                        droneLocationLat = djiFlightControllerCurrentState.getAircraftLocation().getLatitude();
                        droneLocationLng = djiFlightControllerCurrentState.getAircraftLocation().getLongitude();
                        droneLocationAlt = djiFlightControllerCurrentState.getAircraftLocation().getAltitude();
                        double distanceToTarget = Location.distance(droneLocationLat, targetLat, droneLocationLng, targetLon, droneLocationAlt, targetAlt);
                        pitchAngle = (int) Math.round(Math.toDegrees(Math.acos(((double)droneLocationAlt) / distanceToTarget))) - 90.0;

                        rollAngle = djiFlightControllerCurrentState.getAttitude().roll;
                        yawAngle = djiFlightControllerCurrentState.getAttitude().yaw;

                    }
                });

                gimbal.setStateCallback(new GimbalState.Callback() {
                    @Override
                    public void onUpdate(@NonNull GimbalState gimbalState) {
                        gimbalState = gimbalState;

                        Rotation.Builder builder = new Rotation.Builder().mode(RotationMode.ABSOLUTE_ANGLE).time(1);
                        builder.pitch((float)pitchAngle);

                        gimbal.rotate(builder.build(), null);

                    }
                });
            }
        }

        /** The initial waypoint mission.  Set after
         * calling coregistration the first time
         */

        public void initialMission(List<Location.GPSCoor> route ){

            route.remove(0); //Less than 5m returns error (DJI Waypoint)

            Log.d(TAG2, "size of route" + route.size());

            targetLat = route.get(route.size() - 1).getLat();
            targetLon = route.get(route.size() - 1).getLon();
            targetAlt = 0;

            waypointMissionBuilder = new WaypointMission.Builder();
            for(int i = 0; i < route.size(); i++)
            {
                Location.GPSCoor temp = route.get(i);
                Log.d(TAG2, "LAT: " + temp.getLat() + " LON " + temp.getLon() + " ALT " + temp.getElev());
                Waypoint mWaypoint = new Waypoint(temp.getLat(), temp.getLon(), (float)temp.getElev());
                mWaypoint.cornerRadiusInMeters = 1.5f;
                waypointList.add(mWaypoint);
                waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
            }

        }

        /** The reroute mission (if a better coregistration)
         * has been found.
         */

        public void reroute(List<Location.GPSCoor> route){

            waypointList.clear();

            //route.add(new Location.GPSCoor(33.851272,-118.286900,20.0));
            //route.add(new Location.GPSCoor(33.850612, -118.286811,40.0));
            //route.add(new Location.GPSCoor(33.850423, -118.286803,35.0));

            targetLat = route.get(route.size() - 1).getLat();
            targetLon = route.get(route.size() - 1).getLon();
            targetAlt = 0;

            for(int i = 0; i < route.size(); i++)
            {
                Location.GPSCoor temp = route.get(i);
                Waypoint mWaypoint = new Waypoint(temp.getLat(), temp.getLon(), (float)temp.getElev());
                mWaypoint.cornerRadiusInMeters = 1.5f;
                waypointList.add(mWaypoint);
                waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
            }
            DJIError error = getWaypointMissionOperator().loadMission(waypointMissionBuilder.build());
            if (error == null) {
                //setResultToToast("Reroute succeeded");
            } else {
                //setResultToToast("Reroute failed " + error.getDescription());
            }
        }

        public void clearMission()
        {
            waypointList.clear();
        }

        //Add Listener for WaypointMissionOperator
        public void addListener() {
            if (getWaypointMissionOperator() != null){
                getWaypointMissionOperator().addListener(eventNotificationListener);
            }
        }

        public void removeListener() {
            if (getWaypointMissionOperator() != null) {
                getWaypointMissionOperator().removeListener(eventNotificationListener);
            }
        }

        private WaypointMissionOperatorListener eventNotificationListener = new WaypointMissionOperatorListener() {
            @Override
            public void onDownloadUpdate(WaypointMissionDownloadEvent downloadEvent) {

            }

            @Override
            public void onUploadUpdate(WaypointMissionUploadEvent uploadEvent) {

            }

            @Override
            public void onExecutionUpdate(WaypointMissionExecutionEvent executionEvent) {

            }

            @Override
            public void onExecutionStart() {

            }

            @Override
            public void onExecutionFinish(@Nullable final DJIError error) {
                //setResultToToast("Execution finished: " + (error == null ? "Success!" : error.getDescription()));
            }
        };

        public WaypointMissionOperator getWaypointMissionOperator() {
            if (instance == null) {
                if (DJISDKManager.getInstance().getMissionControl() != null){
                    instance = DJISDKManager.getInstance().getMissionControl().getWaypointMissionOperator();
                }
            }
            return instance;
        }

        /** Config parameters of the flight, including things like
         * travel speed, what to do after mission complete, etc.
         */
        public void configWayPointMission(){

            if (waypointMissionBuilder == null){

                waypointMissionBuilder = new WaypointMission.Builder().finishedAction(mFinishedAction)
                        .headingMode(mHeadingMode)
                        .autoFlightSpeed(mSpeed)
                        .maxFlightSpeed(mSpeed)
                        .flightPathMode(WaypointMissionFlightPathMode.CURVED);

            }else
            {
                waypointMissionBuilder.finishedAction(mFinishedAction)
                        .headingMode(mHeadingMode)
                        .autoFlightSpeed(mSpeed)
                        .maxFlightSpeed(mSpeed)
                        .flightPathMode(WaypointMissionFlightPathMode.CURVED);

            }

            if (waypointMissionBuilder.getWaypointList().size() > 0){
                //setResultToToast("Set Waypoint attitude successfully");
            }

            DJIError error = getWaypointMissionOperator().loadMission(waypointMissionBuilder.build());
            if (error == null) {
                //setResultToToast("loadWaypoint succeeded");
            } else {
                //setResultToToast("loadWaypoint failed " + error.getDescription());
            }
        }

        /** Wait for drone ready before uploading the reroute mission */
        public void rerouteWaypointMission(List<Location.GPSCoor> route){

            getWaypointMissionOperator().stopMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError error) {
                    //setResultToToast("Mission Stop: " + (error == null ? "Successfully" : error.getDescription()));
                    while(getWaypointMissionOperator().getCurrentState() != READY_TO_UPLOAD){}
                    reroute(route);
                    while(getWaypointMissionOperator().getCurrentState() != READY_TO_UPLOAD){}
                    uploadWayPointMission();
                    while(getWaypointMissionOperator().getCurrentState() == UPLOADING || getWaypointMissionOperator().getCurrentState() != READY_TO_EXECUTE){}
                    startWaypointMission();
                }
            });

        }
        /** Upload the waypoint mission to drone */
        public void uploadWayPointMission(){
            Log.d(TAG2, "SIZE OF WAYPOINT MISSION IS " + waypointList.size());
            getWaypointMissionOperator().uploadMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError error) {
                    if (error == null) {
                        Log.d(TAG2, "Mission upload successfully!");
                    } else {
                        Log.d(TAG2,"Mission upload failed, error: " + error.getDescription() + " retrying...");
                        getWaypointMissionOperator().retryUploadMission(null);
                    }
                }
            });

        }
        /** Starts the waypoint mission */
        public void startWaypointMission(){

            getWaypointMissionOperator().startMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError error) {
                    //setResultToToast("Mission Start: " + (error == null ? "Successfully" : error.getDescription()));
                }
            });
        }
        /** Stops the waypoint mission */
        public void stopWaypointMission(){

            getWaypointMissionOperator().stopMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError error) {
                    //setResultToToast("Mission Stop: " + (error == null ? "Successfully" : error.getDescription()));
                }
            });

        }

        /** Method for taking photo and saving directly to SDcard.
         * Was used for testing but left here for future use.  Currently
         * not used.
         */
        public void captureAction(){

            final Camera camera = getCameraInstance();
            if (camera != null) {

                SettingsDefinitions.ShootPhotoMode photoMode = SettingsDefinitions.ShootPhotoMode.INTERVAL; // Set the camera capture mode as Single mode
                camera.setShootPhotoMode(photoMode, new CommonCallbacks.CompletionCallback(){
                    @Override
                    public void onResult(DJIError djiError) {
                        if (null == djiError) {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    camera.startShootPhoto(new CommonCallbacks.CompletionCallback() {
                                        @Override
                                        public void onResult(DJIError djiError) {
                                            if (djiError == null) {
                                                //setResultToToast("TAKE PHOTO: success");
                                            } else {
                                                //setResultToToast(djiError.getDescription());
                                            }
                                        }
                                    });
                                }
                            }, 2000);
                        }
                    }
                });
            }
        }

        /** Methods for starting and stopping video recording to SD card.
         * Currently not used.  Left for future use.
         */
        // Method for starting recording
        public void startRecord(CommonCallbacks.CompletionCallback x){

            Camera camera = getCameraInstance();
            if (camera != null) {
                camera.startRecordVideo(x); // Execute the startRecordVideo API
            }
        }

        public void stopRecord(CommonCallbacks.CompletionCallback x){

            Camera camera = getCameraInstance();
            if (camera != null) {
                camera.stopRecordVideo(x);
            }

        }

        /** Switch between the various camera modes */
        public void switchCameraMode(SettingsDefinitions.CameraMode cameraMode){

            Camera camera = getCameraInstance();
            if (camera != null) {
                camera.setMode(cameraMode, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError error) {

                        if (error == null) {
                            //setResultToToast("Switch Camera Mode Succeeded");
                        } else {
                            //setResultToToast(error.getDescription());
                        }
                    }
                });
            }
        }

        /** Get an instance of the current drone's camera */
        public static synchronized Camera getCameraInstance() {

            if (DJIDemoApplication.getProductInstance() == null) return null;

            Camera camera = null;

            if (DJIDemoApplication.getProductInstance() instanceof Aircraft){
                camera = ((Aircraft) DJIDemoApplication.getProductInstance()).getCamera();

            } else if (DJIDemoApplication.getProductInstance() instanceof HandHeld) {
                camera = ((HandHeld) DJIDemoApplication.getProductInstance()).getCamera();
            }

            return camera;
        }

        private void setResultToToast(final String string){
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mActivity, string, Toast.LENGTH_SHORT).show();
                }
            });
        }
        /** Get functions to retrieve associated data.*/
        public double getDroneLocationLat()
        {
            return droneLocationLat;
        }

        public double getDroneLocationLng()
        {
            return droneLocationLng;
        }

        public double getDroneLocationAlt()
        {
            return droneLocationAlt;
        }

        public double getPitchAngle()
        {
            return pitchAngle;
        }

        public double getYawAngle()
        {
            return yawAngle;
        }

        public double getRollAngle()
        {
            return rollAngle;
        }
    }
}
