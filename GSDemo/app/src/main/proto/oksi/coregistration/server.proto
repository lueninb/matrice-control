syntax = "proto3";

package oksi.coregistration;

service Coregistration {
    rpc RegisterToSatellite (CoregistrationRequest) returns (CoregistrationResponse) {}
}

message Coordinate {
    double latitude = 1;
    double longitude = 2;
    float altitude_m = 3;
}

message Pose {
    Coordinate coordinate = 1;

    // Using degrees as unit
    float roll = 2;
    float pitch = 3;
    float yaw = 4;
}

message CoregistrationRequest {
    string mission_id = 1;
    Image image = 2;
    Pose pose_estimate = 3;

    Coordinate target_coordinate = 4;
    MunitionType munition_type = 5;
}

message CoregistrationResponse {
    Pose computed_pose = 1;

    // Simulating a lack of GPS and using coregistration to compute pose,
    // theses coordinates are obtained from:
    // - compute estimated lat/lon from coregistration
    // - computed lat/lon offset to target_coordinate
    // - apply offset to the pose_estimate in the request
    // - then, with this target coordinate, interpolate waypoints for a dive
    repeated Coordinate waypoints = 2;

    // If true, the solution is sufficiently different that the previous
    // list of waypoints is invalid and should be updated.
    // Currently: `dist(last_waypoints[-1], these_waypoints[-1]).m > 10m`
    bool route_update = 3;
}

message Image {
    // Any typical, compressed image format, e.g. jpeg
    bytes data = 1;
}

enum MunitionType {
    UNKNOWN = 0;  // Unset munition type
    DUMMY = 1;  // simple quadratic fit on altitude, linear on lat/lon
    M_81MM = 2;
    M_XM1155 = 3;
}
