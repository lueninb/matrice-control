package com.dji.GSDemo.GoogleMap;

import org.junit.Test;

import oksi.coregistration.Server;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void toStringDoesntCrash() {
        Server.CoregistrationRequest request = Server.CoregistrationRequest.newBuilder()
                .setPoseEstimate(Server.Pose.newBuilder().setPitch(123))
                .build();
        System.out.println(request.toString());

    }
}
